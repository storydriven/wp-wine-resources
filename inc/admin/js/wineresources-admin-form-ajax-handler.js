jQuery( document ).ready( function( $ ) {

        "use strict";
	/**
         * The file is enqueued from inc/admin/class-admin.php.
	 */        
        $( '#wineresources_add_user_meta_ajax_form' ).submit( function( event ) {
            
            event.preventDefault(); // Prevent the default form submit.            
            
            // serialize the form data
            var ajax_form_data = $("#wineresources_add_user_meta_ajax_form").serialize();
            //console.log(ajax_form_data);
            //add our own ajax check as X-Requested-With is not always reliable
            ajax_form_data = ajax_form_data+'&ajaxrequest=true&submit=Submit+Form';
            
            $.ajax({
                url:    params.ajaxurl, // domain/wp-admin/admin-ajax.php
                dataType: 'json',
                type:   'post',                
                data:   ajax_form_data
            })
            
            .done( function( response ) { // response from the PHP action
                //console.log(response);
                $(" #wineresources_form_feedback ").html( "<h2>The request was successful </h2>");
                if(response.wine != ''){
                    $('#winename').append('<option value="'+response.wine_last_id+'">'+response.wine+'</option>');
                    $('#wineRecources-table tbody').append('<tr><td>'+response.wine+'</td></tr>');
                    
                }
                if(response.wine_year != ''){
                    $('#wineyear').append('<option value="'+response.year_last_id+'">'+response.wine_year+'</option>');
                    $('#wineRecources-table thead tr').append('<td>'+response.wine_year+'</td>');
                }
            })
            
            // something went wrong  
            .fail( function() {
                $(" #wineresources_form_feedback ").html( "<h2>Something went wrong.</h2><br>" );                  
            })
        
            // after all this time?
            .always( function() {
                event.target.reset();
            });
        
       });
       $( '#wineresources_del_user_meta_ajax_form' ).submit( function( event ) {
            //alert();
            event.preventDefault(); // Prevent the default form submit.            
            
            // serialize the form data
            var ajax_form_data = $("#wineresources_del_user_meta_ajax_form").serialize();
            
            //add our own ajax check as X-Requested-With is not always reliable
            ajax_form_data = ajax_form_data+'&ajaxrequest=true&submit=Submit+Form';
            //console.log(ajax_form_data);
            $.ajax({
                url:    params.ajaxurl, // domain/wp-admin/admin-ajax.php
                type:   'post',                
                data:   ajax_form_data
            })
            
            .done( function( response ) { // response from the PHP action
                $(" #wineresources_form_feedback ").html( "<h2>The request was successful </h2>" );
                $("#winename option[value='"+response+"']").remove();
                //console.log(response);
                //alert(response);
            })
            
            // something went wrong  
            .fail( function() {
                $(" #wineresources_form_feedback ").html( "<h2>Something went wrong in wine name.</h2><br>" );                  
            })
        
            // after all this time?
            .always( function() {
                event.target.reset();
            });
        
        });

    $( '#wineresources_del_year_meta_ajax_form' ).submit( function( event ) {
            
        event.preventDefault(); // Prevent the default form submit.            
        
        // serialize the form data
        var ajax_form_data = $("#wineresources_del_year_meta_ajax_form").serialize();
        
        //add our own ajax check as X-Requested-With is not always reliable
        ajax_form_data = ajax_form_data+'&ajaxrequest=true&submit=Submit+Form';
        //console.log(ajax_form_data);
        $.ajax({
            url:    params.ajaxurl, // domain/wp-admin/admin-ajax.php
            type:   'post',                
            data:   ajax_form_data
        })
        
        .done( function( response ) { // response from the PHP action
            $(" #wineresources_form_feedback ").html( "<h2>The request was successful </h2>" );
            $("#wineyear option[value='"+response+"']").remove();
        })
        
        // something went wrong  
        .fail( function() {
            $(" #wineresources_form_feedback ").html( "<h2>Something went wrong in wine year.</h2>" );                  
        })
    
        // after all this time?
        .always( function() {
            event.target.reset();
        });
    
    });
        
});

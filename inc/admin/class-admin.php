<?php

namespace Wineresources_Admin_Form\Inc\Admin;

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @link
 * @since      1.0.0
 *
 * @author    Storydriven
 */
class Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * The text domain of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_text_domain    The text domain of this plugin.
	 */
	private $plugin_text_domain;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param    string $plugin_name	The name of this plugin.
	 * @param    string $version	The version of this plugin.
	 * @param	 string $plugin_text_domain	The text domain of this plugin
	 */
	public function __construct( $plugin_name, $version, $plugin_text_domain ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->plugin_text_domain = $plugin_text_domain;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/wineresources-admin-form-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		$params = array ( 'ajaxurl' => admin_url( 'admin-ajax.php' ) );
		wp_enqueue_script( 'wineresources_ajax_handle', plugin_dir_url( __FILE__ ) . 'js/wineresources-admin-form-ajax-handler.js', array( 'jquery' ), $this->version, false );
		wp_localize_script( 'wineresources_ajax_handle', 'params', $params );

	}

	/**
	 * Callback for the admin menu
	 *
	 * @since    1.0.0
	 */
	public function add_plugin_admin_menu() {

		add_menu_page(	__( 'Wine Resources', $this->plugin_text_domain ), //page title
						__( 'Wine Resources', $this->plugin_text_domain ), //menu title
						'manage_options', //capability
						$this->plugin_name, //menu_slug
						array( $this, 'ajax_form_page_content' )
					);
	}


	/*
	 * Callback for the add_submenu_page action hook
	 *
	 * The plugin's HTML Ajax is loaded from here
	 *
	 * @since	1.0.0
	 */
	public function ajax_form_page_content() {
		include_once( 'views/partials-ajax-form-view.php' );
	}

	/*
	 * Callback for the load-($ajax_form_page_hook)
	 * Called when the plugin's submenu Ajax form page is loaded
	 *
	 * @since	1.0.0
	 */
	public function loaded_ajax_form_submenu_page() {
		// called when the particular page is loaded.
	}

	/**
	 *
	 * @since    1.0.0
	 */

	public function the_form_response() {

		if( isset( $_POST['wineresources_add_user_meta_nonce'] ) && wp_verify_nonce( $_POST['wineresources_add_user_meta_nonce'], 'wineresources_add_user_meta_form_nonce') ) {

			function rrmdir($dir) {
				if (is_dir($dir)) {
				  $objects = scandir($dir);
				  foreach ($objects as $object) {
					if ($object != "." && $object != "..") {
					  if (filetype($dir."/".$object) == "dir")
						 rrmdir($dir."/".$object);
					  else unlink($dir."/".$object);
					}
				  }
				  reset($objects);
				  rmdir($dir);
				}
			}

			function createDirectory(){

				$upload = wp_upload_dir();
				$main_dir = $upload['basedir'].'/wineResources';
				if (! is_dir($main_dir)) {
					mkdir( $main_dir, 0777 );
				}

				global $wpdb;
				$wineyears = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}wineyear ORDER BY year DESC");
				$winenames = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}winename");


				//$upload = wp_upload_dir();
				foreach ($winenames as $wine) {
					$wineFolder = '';
					$upload_check = $upload['basedir'].'/wineResources/';
					$str2 = str_replace(" ", "-", strtolower($wine->name));
					$wineFolder = $upload_check."/".$str2;
					if (! is_dir($wineFolder)) {
						mkdir( $wineFolder, 0755 );
					}
					foreach ($wineyears as $year) {
						$yearFolder = '';
						$yearFolder = $wineFolder."/".$year->year;
						if (! is_dir($yearFolder)) {
							mkdir( $yearFolder, 0755 );
						}
					}
				}
			}


			if( isset( $_POST['add'] )){

				$wineInputName = sanitize_text_field( $_POST['wineresources']['wine'] );
				$wineyear = sanitize_text_field( $_POST['wineresources']['year'] );
				$wineresources_user =  get_user_by( 'login',  $_POST['wineresources']['user_select'] );
				$wineresources_user_id = absint( $wineresources_user->ID ) ;


				if( isset( $_POST['ajaxrequest'] ) && $_POST['ajaxrequest'] === 'true' ) {

					global $wpdb;
					$wine_name = $wpdb->prefix . 'winename';
					$wine_year = $wpdb->prefix . 'wineyear';

					$upload = wp_upload_dir();

					if( !empty( $_POST['wineresources']['wine'] )){

						$wpdb->insert($wine_name , array(
							'name' => $wineInputName,
							'time' => current_time("mysql"),
						));
						$lastid_wine = $wpdb->insert_id;
						$str = str_replace(" ", "-", strtolower($wineInputName));
						$upload_dir = $upload['basedir'].'/wineResources/'.$str;
						if (! is_dir($upload_dir)) {
							mkdir( $upload_dir, 0755 );
						}

						createDirectory();


					}

					/* Add Years */
					if( !empty( $_POST['wineresources']['year'] )){
						$wpdb->insert($wine_year , array(
							'year' => $wineyear,
							'time' => current_time("mysql"),
						));
						$lastid_year = $wpdb->insert_id;
						createDirectory();
					}

					$arr = array();
					$arr = array('wine'=>$wineInputName,'wine_last_id'=>$lastid_wine,'wine_year'=>$wineyear,'year_last_id'=>$lastid_year);
					echo json_encode($arr);

					wp_die();
				}
			}
			if( isset( $_POST['select_wine'] )){
				$select_wine = sanitize_key( $_POST['select_wine'] );

				if( isset( $_POST['ajaxrequest'] ) && $_POST['ajaxrequest'] === 'true' ) {

					global $wpdb;
					$wine_name = $wpdb->prefix . 'winename';

					$winename = $wpdb->get_results("SELECT name FROM $wine_name WHERE $wine_name.id = $select_wine");
					$deleted_wine = $winename[0]->name;

					$wpdb->delete( $wine_name, array( 'id' => $select_wine ) );

					$deleted_wine_name = str_replace(" ", "-", strtolower($deleted_wine));

					/* Delete Wine Folder from Directory */
					$upload = wp_upload_dir();
					$wine_folder_dir = $upload['basedir'].'/wineResources/'.$deleted_wine_name;
					rrmdir($wine_folder_dir);

					echo $select_wine;

					wp_die();
				}
				//echo 'action del';
			}

			if( isset( $_POST['select_year'] )){
				$select_year = sanitize_key( $_POST['select_year'] );

				if( isset( $_POST['ajaxrequest'] ) && $_POST['ajaxrequest'] === 'true' ) {

					global $wpdb;
					$wine_year = $wpdb->prefix . 'wineyear';

					$wineyears = $wpdb->get_results("SELECT * FROM $wine_year");
					$winenames = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}winename");

					$deletedWineYear = $wpdb->get_results("SELECT year FROM $wine_year WHERE $wine_year.id = $select_year");

					/* Delete Wine Year from Database */
					$wpdb->delete( $wine_year, array( 'id' => $select_year ) );

					$upload = wp_upload_dir();
					foreach ($winenames as $wine) {
						$wineFolder = '';
						$folder_check = $upload['basedir'].'/wineResources';
						$str2 = str_replace(" ", "-", strtolower($wine->name));
						$wineFolder = $folder_check."/".$str2;
						foreach ($wineyears as $year) {
							$yearFolder = '';
							$yearFolder = $wineFolder."/".$year->year;
							if ($deletedWineYear[0]->year == $year->year) {
								rrmdir($yearFolder);
							}
						}
					}

					echo $select_year;

					wp_die();
				}
			}


			// server response
			$admin_notice = "success";
			$this->custom_redirect( $admin_notice, $_POST );
			exit;
		}
		else {
			wp_die( __( 'Invalid nonce specified', $this->plugin_name ), __( 'Error', $this->plugin_name ), array(
						'response' 	=> 403,
						'back_link' => 'admin.php?page=' . $this->plugin_name,

				) );
		}


	}


	/**
	 * Redirect
	 *
	 * @since    1.0.0
	 */
	public function custom_redirect( $admin_notice, $response ) {
		wp_redirect( esc_url_raw( add_query_arg( array(
			'wineresources_admin_add_notice' => $admin_notice,
			'wineresources_response' => $response,
		),
			admin_url('admin.php?page='. $this->plugin_name )
		) ) );

	}


	/**
	 * Print Admin Notices
	 *
	 * @since    1.0.0
	 */
	public function print_plugin_admin_notices() {
		  if ( isset( $_REQUEST['wineresources_admin_add_notice'] ) ) {
			if( $_REQUEST['wineresources_admin_add_notice'] === "success") {
				$html =	'<div class="notice notice-success is-dismissible">
							<p><strong>The request was successful. </strong></p><br>';
				$html .= '<pre>' . htmlspecialchars( print_r( $_REQUEST['wineresources_response'], true) ) . '</pre></div>';
				echo $html;
			}

			// handle other types of form notices

		  }
		  else {
			  return;
		  }

	}


}

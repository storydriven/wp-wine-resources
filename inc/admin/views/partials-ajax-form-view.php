<?php

/**
 * The form to be loaded on the plugin's admin page
 */
	if( current_user_can( 'edit_users' ) ) {

		$upload = wp_upload_dir();
		$upload_dir = $upload['basedir'].'/wineResources';

		// Populate the dropdown list with exising users.
        $dropdown_html = '<select required id="wineresources_user_select" name="wineresources[user_select]">
                            <option value="">'.__( 'Select a User', $this->plugin_text_domain ).'</option>';
        $wp_users = get_users( array( 'fields' => array( 'user_login', 'display_name' ) ) );

		foreach ( $wp_users as $user ) {
			$user_login = esc_html( $user->user_login );
			$user_display_name = esc_html( $user->display_name );

			$dropdown_html .= '<option value="' . $user_login . '">' . $user_login . ' (' . $user_display_name  . ') ' . '</option>' . "\n";
		}
        $dropdown_html .= '</select>';

		// Generate a custom nonce value.
		$wineresources_add_meta_nonce= wp_create_nonce( 'wineresources_add_user_meta_form_nonce' );
		$wineresources_del_meta_nonce = wp_create_nonce( 'wineresources_del_user_meta_form_nonce' );

		function sortWineYears($years) {
			usort($years, function($x, $y) {
				$x = $x->year;
				$y = $y->year;
				if ($x > 50 && $y < 50) {
							return 1;
					} else if ($x < 50 && $y > 50) {
							return -1;
					} else {
							return $y - $x;
					}
			});
			return $years;
		}

		// Build the Form
?>
		<h2><?php _e( 'Wine Resources', $this->plugin_name ); ?></h2>
		<div class="table-wrap">
			<table id="wineRecources-table" class="responsive-table-input-matrix">
				<thead>
					<tr>
						<td></td>
						<?php
							global $wpdb;

							//$results = $wpdb->get_results( $wpdb->prepare("SELECT * FROM wp_wineresources"));

							$wineyears = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}wineyear ORDER BY year DESC");
							$winenames = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}winename");
							$wineyears = sortWineYears($wineyears);
							foreach ($wineyears as $year) {
								echo '<td>'.$year->year.'</td>';
							}
						?>
					</tr>
				</thead>
				<tbody>
					<?php
						foreach ($winenames as $wine) {
							echo '<tr>';
							echo '<td>'.$wine->name.'</td>';
							$str2 = str_replace(" ", "-", strtolower($wine->name));
							$folderName = $upload_dir."/".$str2;
								foreach ($wineyears as $year) {
									$subFolder = $folderName."/".$year->year;
									$subFolder = str_replace("\\", "\\\\", $subFolder);
									$files = glob($subFolder."/*.*");
									//print_r($files);
									if(!empty($files)){
										echo '<td><input class="styled-checkbox" id="styled-checkbox-1-1" type="checkbox" value="value1"  disabled="disabled" checked="checked"><label for="styled-checkbox-1-1"></label></td>';
									}else{
										echo '<td> <input class="styled-checkbox" id="styled-checkbox-1-2" type="checkbox" value="value1"  disabled="disabled">
										<label for="styled-checkbox-1-2"></label></td>';
									}
								}
							echo '</tr>';
						}
					?>
				</tbody>
			</table>
		</div>
		<div class="wineresources_add_user_meta_form">

		<form action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" method="post" id="wineresources_add_user_meta_ajax_form" >

			<?php //echo $dropdown_html; ?>
			<input type="hidden" name="action" value="wineresources_form_response">
			<input type="hidden" name="add" value="add">
			<input type="hidden" name="wineresources_add_user_meta_nonce" value="<?php echo $wineresources_add_meta_nonce?>" />
			<div class="col">
				<br>
				<label for="<?php echo $this->plugin_name; ?>-wine"> <?php _e('Add Wine', $this->plugin_name); ?> </label><br>
				<input id="<?php echo $this->plugin_name; ?>-wine" type="text" name="<?php echo "wineresources"; ?>[wine]" value="" placeholder="<?php _e('Wine Name', $this->plugin_name);?>" /><br>
			</div>
			<div class="col">
				<label for="<?php echo $this->plugin_name; ?>-year"> <?php _e('Add Wine Year', $this->plugin_name); ?> </label><br>
				<input id="<?php echo $this->plugin_name; ?>-year" type="number" name="<?php echo "wineresources"; ?>[year]" value="" placeholder="<?php _e('Wine Year', $this->plugin_name);?>"/><br>
			</div>
			<p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Add"></p>
		</form>

		<div id="wineresources_form_feedback"></div>

		<?php
			//global $wpdb;
			$results = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}winename");

			?>
			<form action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" method="post" id="wineresources_del_user_meta_ajax_form" >
			<input type="hidden" name="action" value="wineresources_form_response">
			<input type="hidden" name="wineresources_add_user_meta_nonce" value="<?php echo $wineresources_add_meta_nonce?>" />
			<?php
				echo '<select name="select_wine" id="winename">';
					echo '<option value="">Select Wine</option>';
				foreach ($results as $wine) {
					echo '<option value="'.$wine->id.'">'.$wine->name.'</option>';
				}
			?>
			<input type="submit" name="submit" id="submit" class="button button-primary" value="Remove Wine">


<?php
			echo '</select></form>'; ?>
			<form action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" method="post" id="wineresources_del_year_meta_ajax_form" >
			<input type="hidden" name="action" value="wineresources_form_response">
			<input type="hidden" name="wineresources_add_user_meta_nonce" value="<?php echo $wineresources_add_meta_nonce?>" />
			<?php
				$years = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}wineyear");
				echo '<select name="select_year" id="wineyear">';
				echo '<option value="">Select Year</option>';
				foreach ($years as $year) {
					echo '<option value="'.$year->id.'">'.$year->year.'</option>';
				}
				echo '</select>';
			?>
		<input type="submit" name="year_del" id="submit" class="button button-primary" value="Remove Year">
		</form>

<a href="<?php echo site_url(); ?>/wp-admin/admin.php?page=wp_file_manager#elf_l1_Lw" class="button button-primary explore">Explore Wines</a>

		</div>
	<?php
	}
	else {
	?>
		<p> <?php __("You are not authorized to perform this operation.", $this->plugin_name) ?> </p>
	<?php
	}
?>

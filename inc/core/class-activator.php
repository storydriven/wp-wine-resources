<?php

namespace Wineresources_Admin_Form\Inc\Core;

/**
 * Fired during plugin activation
 *
 * This class defines all code necessary to run during the plugin's activation.

 * @link       
 * @since      1.0.0
 *
 * @author     Storydriven
 */

class Activator {

	/**
	 * Short Description.
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

		$min_php = '5.6.0';

		// Check PHP Version and deactivate & die if it doesn't meet minimum requirements.
		if ( version_compare( PHP_VERSION, $min_php, '<' ) ) {
					deactivate_plugins( plugin_basename( __FILE__ ) );
			wp_die( 'This plugin requires a minmum PHP Version of ' . $min_php );
		}

		global $wpdb;

		$wine_name = $wpdb->prefix . 'winename';
		$wine_year = $wpdb->prefix . 'wineyear';
		
		$charset_collate = $wpdb->get_charset_collate();

		$wineNameTable = "CREATE TABLE $wine_name (
		id mediumint(9) NOT NULL AUTO_INCREMENT,
		time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
		name varchar(50) NOT NULL,
		PRIMARY KEY  (id)
		) $charset_collate;";

		$wineYearTable = "CREATE TABLE $wine_year (
		id mediumint(9) NOT NULL AUTO_INCREMENT,
		time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
		year varchar(50) NOT NULL,
		PRIMARY KEY  (id)
		) $charset_collate;";

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $wineNameTable );
		dbDelta( $wineYearTable );

		$upload = wp_upload_dir();
		$upload_dir = $upload['basedir'].'/wineResources';
		if (! is_dir($upload_dir)) {
			mkdir( $upload_dir, 0777 );
		}	

	}

}

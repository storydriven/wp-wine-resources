<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       
 * @since      1.0.0
 *
 * @package    Wineresources_Admin_Form
 * @subpackage Wineresources_Admin_Form/inc/frontend/views
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
